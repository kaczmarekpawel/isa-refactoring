//
// Credits to: http://elijahmanor.com/talks/js-smells
//


const CONSONANTS = ['th', 'qu', 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k',
    'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'];
const VOWELS = ['a', 'e', 'i', 'o', 'u'];
const ENDING = 'ay';

let isValid = word => startsWithVowel(word) || startsWithConsonant(word);

let startsWithVowel = word => !!~VOWELS.indexOf(word[0]);

let startsWithConsonant = word => !!~CONSONANTS.indexOf(word[0]);

let getConsonants = word => CONSONANTS.reduce((memo, char) => {
    if (word.startsWith(char)) {
        memo += char;
        word = word.substr(char.length);
    }
    return memo;
}, '');

function englishToPigLatin(english = '') {
    if (isValid(english)) {
        if (startsWithVowel(english)) {
            english += ENDING;
        } else {
            let letters = getConsonants(english);
            english = `${english.substr(letters.length)}${letters}${ENDING}`;
        }
    }
    return english;
}
